# Moviesfront

Projekt został wygenerowany za pomocą [Angular CLI](https://github.com/angular/angular-cli) ver. 9.1.0.

## Wymagania do uruchomienia

[NodeJS](https://nodejs.org/en/download/) w wersji 11+.
######
[Angular CLI](https://cli.angular.io/)

## Uruchamianie

Należy sklonować to repozytorium za pomocą `git clone https://TheSigillite@bitbucket.org/TheSigillite/anonforum_front.git`
######
Należy przejść do folderu ze sklonowanym programem i pobrać zależności za pomocą komendy `npm install`
######
Program można uruchomić za pomocą komendy `ng serve`. Strona dostępna będzie pod aresem `localhost:4200/movies`

## Zastosowane technologie
NodeJS
######
Type Script
######
Angular
