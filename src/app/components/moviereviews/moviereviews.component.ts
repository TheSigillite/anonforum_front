import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ReviewserviceService} from '../../services/reviewservice.service';
import {StorageserviceService} from '../../services/storageservice.service';
import {MovieserviceService} from '../../services/movieservice.service';

@Component({
  selector: 'app-moviereviews',
  templateUrl: './moviereviews.component.html',
  styleUrls: ['./moviereviews.component.css']
})
export class MoviereviewsComponent implements OnInit {
  reviewdmovie: any;
  reviews: any;
  rev: string;
  isadm: any;
  login: string;
  constructor(private aroute: ActivatedRoute, private rservice: ReviewserviceService, private mservice: MovieserviceService) { }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.aroute.paramMap.subscribe(params => {
      const movieid = Number(params.get('id'));
      this.mservice.getOne(movieid).subscribe(movie => {
        this.reviewdmovie = movie;
        this.rservice.getreviews(this.reviewdmovie.movie_id).subscribe(response => {
          console.log(response);
          this.reviews = response;
          console.log(this.reviews);
        });
      });
    });
    try{
      this.login = localStorage.getItem('movieslogin');
      this.isadm = JSON.parse(localStorage.getItem('ismod'));
    }catch (e) {
      if (e instanceof TypeError){
        this.isadm = undefined;
        this.login = undefined;
      }
    }

    this.rservice.getreviews(this.reviewdmovie.movie_id).subscribe(response => {
      console.log(response);
      this.reviews = response;
      console.log(this.reviews);
    }, error => alert(error.error.details));
  }

  submitNewReview(){
    this.rservice.newreview({
      login: localStorage.getItem('movieslogin'),
      passwd: localStorage.getItem('moviespass'),
      movie_id: this.reviewdmovie.movie_id,
      rev: this.rev
    }).subscribe(response => {
      window.alert(response.details);
      window.location.reload();
    }, error => {
      let err: any = error;
      window.alert(err.error.details);
    });
  }

  deleteReview(revi: number){
    this.rservice.deletereview({
      login: localStorage.getItem('movieslogin'),
      passwd: localStorage.getItem('moviespass'),
      rev_id: revi
    }).subscribe(response =>{
      let r: any = response;
      window.alert(response.details);
      window.location.reload();
    }, error => {
      let er: any = error;
      window.alert(er.error.details);
    });
  }
}
