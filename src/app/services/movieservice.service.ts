import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { AddMovie } from '../interfaces/add-movie';
import { EditMovie } from '../interfaces/edit-movie';
import { DeleteMovie } from '../interfaces/delete-movie';
import { Movie } from '../interfaces/movie';
import { ResponseDTO } from '../interfaces/responsedto';

@Injectable()
export class MovieserviceService {
  private url = 'http://localhost:8080/movies';
  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get<Movie[]>(this.url);
  }

  // tslint:disable-next-line:variable-name
  getOne(movie_id: number){
    return this.http.get<Movie>(this.url + '/' + movie_id);
  }

  addMovie(newmovie: AddMovie){
    return this.http.post<ResponseDTO>(this.url, newmovie);
  }

  // tslint:disable-next-line:variable-name
  editMovie(editmovie: EditMovie, movie_id: number){
    return this.http.put<ResponseDTO>(this.url + '/' + movie_id, editmovie);
  }

  deleteMovie(deleteLoad: DeleteMovie){
    let userHeaders: HttpHeaders = new HttpHeaders();
    userHeaders = userHeaders.append('Usr-Login', deleteLoad.login);
    userHeaders = userHeaders.append('Usr-Pass', deleteLoad.passwd);
    return this.http.delete<ResponseDTO>(this.url + '/' + deleteLoad.movie_id , { headers: userHeaders });
  }
}
