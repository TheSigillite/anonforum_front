import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Review} from '../interfaces/review';
import { NewReview } from '../interfaces/new-review';
import { ResponseDTO } from '../interfaces/responsedto';
import { DeleteReview } from '../interfaces/delete-review';

@Injectable()
export class ReviewserviceService {
  url = 'http://localhost:8080/';
  constructor(private httpclient: HttpClient) { }

  getreviews(movieid: number){
    return this.httpclient.get<Review[]>(this.url + 'movies/' + movieid + '/reviews');
  }

  newreview(review: NewReview){
    return this.httpclient.post<ResponseDTO>(this.url + 'reviews', review);
  }

  deletereview(todelete: DeleteReview){
    let userHeaders: HttpHeaders = new HttpHeaders();
    userHeaders = userHeaders.append('Usr-Login', todelete.login);
    userHeaders = userHeaders.append('Usr-Pass', todelete.passwd);
    return this.httpclient.delete<ResponseDTO>(this.url + 'reviews/' + todelete.rev_id, { headers: userHeaders});
  }
}
