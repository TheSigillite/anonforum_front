export interface AddMovie {
    login: string;
    passwd: string;
    movieDTO: {
      movie_id: number;
      title: string;
      cover: string;
      director: string;
      premiere: number; };
}
