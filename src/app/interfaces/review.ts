export interface Review {
  rev_id: number;
  movie_id: number;
  login: string;
  rev: string;
}
